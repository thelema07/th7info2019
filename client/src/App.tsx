import React, { Fragment } from "react";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import FormIndex from "./form-features/FormIndex";

import "./App.scss";
import "materialize-css/sass/materialize.scss";

import Topbar from "./layout/topbar/Topbar";
import Landing from "./layout/main/Landing/Landing";

// Custom Form
import BasicInfoForm from "./form-features/form-catalog/form-1/BasicInfoForm";
import BasicAccorionForm from "./form-features/form-catalog/form-2/BasicAccordionForm";

const history = createBrowserHistory();

const App: React.FC = () => {
  return (
    <div className="App">
      {/* {alert("Must implement likes in every form to rate access")} */}
      <Router history={history}>
        <Fragment>
          <Topbar />
          <Route exact path="/" component={Landing} />
          <Switch>
            <Route exact path="/form-index" component={FormIndex} />
            <Route exact path="/form-1" component={BasicInfoForm} />
            <Route exact path="/form-2" component={BasicAccorionForm} />
          </Switch>
        </Fragment>
      </Router>
    </div>
  );
};

export default App;
