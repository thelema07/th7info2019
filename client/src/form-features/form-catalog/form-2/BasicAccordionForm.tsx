import React, { useEffect, Fragment } from "react";
import M from "materialize-css";

import styles from "./BasicAccordionForm.module.scss";
import "../form-catalog.scss";

const BasicAccordionForm = () => {
  useEffect(() => {
    const collapse = document.addEventListener("DOMContentLoaded", function() {
      var elems = document.querySelectorAll(".collapsible");
      M.Collapsible.init(elems);
    });
  }, []);
  // the effect and lifecicle are failing now this must be fixed using hooks
  return (
    <Fragment>
      <h3 className={styles.formStyles}>Basic Form</h3>
      <section className={styles.formBasicStyles}>
        <form>
          <ul className="collapsible">
            <li>
              <div className="collapsible-header">
                <i className="material-icons">filter_drama</i>First
              </div>
              <div className="collapsible-body">
                <span>Lorem ipsum dolor sit amet.</span>
              </div>
            </li>
            <li>
              <div className="collapsible-header">
                <i className="material-icons">place</i>Second
              </div>
              <div className="collapsible-body">
                <span>Lorem ipsum dolor sit amet.</span>
              </div>
            </li>
            <li>
              <div className="collapsible-header">
                <i className="material-icons">whatshot</i>Third
              </div>
              <div className="collapsible-body">
                <span>Lorem ipsum dolor sit amet.</span>
              </div>
            </li>
          </ul>
        </form>
      </section>
    </Fragment>
  );
};

export default BasicAccordionForm;
