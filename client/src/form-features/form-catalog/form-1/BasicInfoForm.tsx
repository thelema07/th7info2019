import React, { useEffect, Fragment } from "react";
import M from "materialize-css";

import styles from "./BasicInfoForm.module.scss";
import "../form-catalog.scss";

const BasicInfoForm = () => {
  useEffect(() => {
    M.AutoInit();
  }, []);

  return (
    <Fragment>
      <div className={styles.formContainer}>
        <h3 className={styles.formStyles}>Basic Form</h3>
        <section className={styles.formBasicStyles}>
          <div className="row">
            <form className="col s12">
              <div className="row">
                <div className="input-field col s6">
                  <input id="first_name" type="text" className="validate" />
                  <label htmlFor="first_name">First Name</label>
                </div>
                <div className="input-field col s6">
                  <input id="last_name" type="text" className="validate" />
                  <label htmlFor="last_name">Last Name</label>
                </div>
              </div>

              <div className="row">
                <div className="input-field col s6">
                  <input id="email" type="text" className="validate" />
                  <label htmlFor="email">Email</label>
                </div>
                <div className="input-field col s6">
                  <input id="phone" type="text" className="validate" />
                  <label htmlFor="phone">Phone</label>
                </div>
              </div>

              <div className="row">
                <div className="input-field col s6">
                  <input id="zipcode" type="text" className="validate" />
                  <label htmlFor="zipcode">Zip Code</label>
                </div>
                <div className="input-field col s6">
                  <select>
                    <option value="" disabled selected>
                      Choose your option
                    </option>
                    <option value="1">Option 1</option>
                    <option value="2">Option 2</option>
                    <option value="3">Option 3</option>
                  </select>
                  <label>Country</label>
                </div>
              </div>

              <div className="row">
                <div className="input-field col s12">
                  <p className="left">
                    <label>
                      <input className="with-gap" name="group1" type="radio" />
                      <span>Male</span>
                    </label>
                    <label>
                      <input className="with-gap" name="group1" type="radio" />
                      <span>Female</span>
                    </label>
                  </p>
                </div>
              </div>

              <div className="row">
                <div className="input-field col s6">
                  <input id="person_id" type="text" className="validate" />
                  <label htmlFor="person_id">Person Id</label>
                </div>
                <div className="input-field col s6">
                  <input id="tax_id" type="text" className="validate" />
                  <label htmlFor="tax_id">Tax Id</label>
                </div>
              </div>

              <div className="row">
                <div className="row">
                  <div className="input-field col s12">
                    <textarea id="textarea1" className="materialize-textarea" />
                    <label htmlFor="textarea1">Short Description</label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="input-field col s12">
                  <div className="left">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Quisquam nemo ut corporis dolorem magnam, possimus natus
                    aperiam eaque eos beatae voluptatem, animi neque optio
                    quibusdam cum pariatur ipsam mollitia reprehenderit
                    laboriosam sed necessitatibus? Repellendus aut odio nihil
                    sapiente aspernatur animi?
                  </div>
                  <p className="left">
                    <label>
                      <input
                        type="checkbox"
                        className="filled-in"
                        checked={true}
                      />
                      <span>I Agree to the terms and consditions</span>
                    </label>
                  </p>
                </div>
              </div>

              <div className="row">
                <div className="input-field col s12">
                  <button
                    className="btn waves-effect waves-light"
                    type="submit"
                    name="action"
                  >
                    Submit
                    <i className="material-icons right">send</i>
                  </button>
                </div>
              </div>

              {/* <div className="row">
              <div className="input-field col s12">
                <input
                  disabled
                  value="I am not editable"
                  id="disabled"
                  type="text"
                  className="validate"
                />
                <label htmlFor="disabled">Disabled</label>
              </div>
            </div> */}
              {/* <div className="row">
              <div className="input-field col s12">
                <input id="password" type="password" className="validate" />
                <label htmlFor="password">Password</label>
              </div>
            </div>
            <div className="row">
              <div className="input-field col s12">
                <input id="email" type="email" className="validate" />
                <label htmlFor="email">Email</label>
              </div>
            </div> */}
              {/* <div className="row">
              <div className="col s12">
                This is an inline input field:
                <div className="input-field inline">
                  <input id="email_inline" type="email" className="validate" />
                  <label htmlFor="email_inline">Email</label>
                  <span
                    className="helper-text"
                    data-error="wrong"
                    data-success="right"
                  >
                    Helper text
                  </span>
                </div>
              </div>
            </div> */}
            </form>
          </div>
        </section>
      </div>
    </Fragment>
  );
};

export default BasicInfoForm;
