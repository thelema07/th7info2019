import React, { useEffect, Fragment } from "react";
import M from "materialize-css";
import { Link } from "react-router-dom";

import styles from "./FormIndex.module.scss";

const FormIndex = () => {
  const titles = ["Home", "Advanced Forms", "Gadgets"];

  useEffect(() => {
    M.AutoInit();
  }, []);

  const cardInfo = [
    {
      title: "Basic",
      text:
        "A very simple form with Material design with the basic features of a form",
      link: "/form-1"
    },
    {
      title: "Tabs",
      text:
        "A very simple form with Material design with the basic features of a form",
      link: "/form-2"
    },
    {
      title: "4-steps",
      text:
        "A very simple form with Material design with the basic features of a form",
      link: "/form-1"
    },
    {
      title: "Basic",
      text:
        "A very simple form with Material design with the basic features of a form",
      link: "/form-1"
    },
    {
      title: "Basic",
      text:
        "A very simple form with Material design with the basic features of a form",
      link: "/form-1"
    },
    {
      title: "Basic",
      text:
        "A very simple form with Material design with the basic features of a form",
      link: "/form-1"
    }
  ];

  return (
    <Fragment>
      <div className={styles.mainFormIndex}>
        <h2 className={styles.advancedFormTitle}>Form Portfolio</h2>
        <section className={styles.formCardStyles}>
          <div className="row">
            {cardInfo.map((card, index) => (
              <div className="col s12 m4">
                <div className="card grey darken-4">
                  <div className="card-content orange-text">
                    <h3 className={styles.titleCardStyles}>{card.title}</h3>
                    <p>{card.text}</p>
                  </div>
                  <div className="card-action">
                    <Link to={card.link}>{card.title}</Link>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </section>
      </div>
    </Fragment>
  );
};

export default FormIndex;
