import React, { useEffect, Fragment } from "react";
import { Link } from "react-router-dom";
import M from "materialize-css";

import styles from "./Topbar.module.scss";

const Topbar = () => {
  const navLinks = [
    { title: "Home", link: "/", icon: "home" },
    { title: "Advanced Forms", link: "/form-index", icon: "developer_mode" },
    { title: "Gadgets", link: "/form-index", icon: "list_alt" }
  ];
  const routes = ["/", "/form-index", "/gadgets"];
  useEffect(() => {
    M.AutoInit();
  }, []);

  return (
    <Fragment>
      <nav className={styles.mainNav}>
        <div className="nav-wrapper grey darken-4">
          <div className={styles.navStyles}>
            <ul>
              {navLinks.map((nav, index) => (
                <li className={styles.linkList} key={nav.title}>
                  <Link className="orange-text" to={nav.link}>
                    <i className="material-icons">{nav.icon}</i>
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </nav>
    </Fragment>
  );
};

export default Topbar;
