import React, { useEffect, Fragment } from "react";
import M from "materialize-css";

import styles from "./Landing.module.scss";
import "./Landing.scss";

const Landing = () => {
  return (
    <Fragment>
      <div className={styles.landingStyles}>
        <h1 className="landing-title">Th7</h1>
        <h3>Web Portfolio</h3>
        <h6>
          a showcase of features using Material Design <small>by Google</small>
        </h6>
      </div>
    </Fragment>
  );
};

export default Landing;
